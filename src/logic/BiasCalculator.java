package com.gorou21.algorithm.logic;

import com.gorou21.algorithm.types.*;
import java.util.Date;

/**
 * Created by Gorou on 06.07.2017.
 */

public class BiasCalculator {

    /* Original calculation. Until DB interaction works, this is pointless.
    public static int run(Patient patient, int givenSymptomsLength, Disease disease, int matches, boolean history)
    {
        float bias = 0.0f;
        int symptomsCount = disease.getSymptomsCount();

	// Add bias from history
        if (history)
        {bias += biasFromHistory(patient, disease);}

        bias -= (float) matches / symptomsCount;
        bias += (float) matches / givenSymptomsLength;

        if (matches == givenSymptomsLength && givenSymptomsLength == symptomsCount)
        {return 100;}

        return (int) (bias*100);
    }*/

    // Currently usable calculation procedure. Doesn't account for patient history.
    public static int run(int givenSymptomsLength, Disease disease, int matches)
    {
        float bias = 0.0f;
        int symptomsCount = disease.getSymptomsCount();

        // Subtract percentage of superfluous symptoms in an illness from bias.
        bias -= (float) matches / symptomsCount;

        // Add percentage of correct matches from the given symptoms to bias.
        bias += (float) matches / givenSymptomsLength;

        // if everything fits perfectly, return 100%
        if (matches == givenSymptomsLength && givenSymptomsLength == symptomsCount || bias > 100)
        {return 100;}

        if (bias < 100) {return 0;}

        return (int) (bias*100);
    }

    /* Adds an arbitrary amount of bias based on the months between occurences of given diseases.
    public static float biasFromHistory(Patient patient, Disease disease)
    {
        float bias = 0.0f;

        for (Diagnostic d : patient.getHistory())
        {
            if (d.getDisease().toString().equalsIgnoreCase(disease.toString()))
            {
                bias += returnDateBias(d);
            }
        }

        return bias;
    }*/

    /* Arbitrarily returns a bias value based on the timespan between occurences of a disease.
    public static double returnDateBias(Diagnostic d)
    {
        Date then = d.getDate();
        Date now = new Date();
	
	// Get the difference between when the given Diagnostic was issued and the current time.
	// Uses deprecated Date methods to avert possible compatibility issues with older Android versions.
        int difference = (now.getMonth() + (now.getYear()*12)) - (then.getMonth() + (then.getYear()*12));

	// Returns value based on amount of months passed since the last diagnosis. Probably really poor in execution.
        if (difference == 18) {return 0.05;}
        if (difference >= 12) {return 0.10;}
        if (difference >= 9) {return 0.15;}
        if (difference >= 6) {return 0.30;}
        if (difference >= 3) {return 0.45;}
        if (difference >= 1) {return 0.60;}
        else return 0;
    }*/
}
