package com.gorou21.algorithm.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import com.gorou21.algorithm.types.*;

public class DiagnosticsTool {
    // Storage variables
    private static ArrayList<Disease> diseases = new ArrayList<Disease>();
    private static int givenSymptomsLength;
    private static Diagnostic result = new Diagnostic(new Disease("Run the algorithm first!", null, "Run the algorithm first!"),0);

    // Sends results of diagnostic.
    public static String fetchResult() {return result.toString();}
    public static String fetchTreatment() {return result.getDisease().getTreatment();}

    // This was supposed to fetch all the data about diseases from the DB but it
    // currently just takes disease data from the TestData class and inserts them into the diseases ArrayList.
    public static void initData()
    {
        for (String d : TestData.getDiseases())
        {
            diseases.add(TestData.disease(d));
        }
    }

    // Checks if a given disease's list of symptoms contains the given symptom.
    public boolean containsSymptom(Disease disease, String symptom)
    {
	for(String s : disease.getSymptoms())
	{
		if (s.equalsIgnoreCase(symptom))
		{return true;}
	}
		return false;
	}

    // Compiles a list of possible Diagnostics and returns the first and most probable element
    public static Diagnostic diagnose(ArrayList<String> givenSymptoms) {
		List<Diagnostic> diagnostics = new ArrayList<Diagnostic>();

		// Determine the length of the givenSymptoms ArrayList
		for (String s : givenSymptoms) {
			++givenSymptomsLength;
		}

		for (Disease d : diseases) {
			int matches = 0;

			for (String s : givenSymptoms) {
				if (containsSymptom(d, s)) {++matches;}
			}

			// If the disease matches at least one symptom, add it to the diagnostics ArrayList.
			if (matches > 0) {
				diagnostics.add(new Diagnostic(d, BiasCalculator.run(givenSymptomsLength, d, matches)));
			}
		}

		// Comparator for sorting diagnostics ArrayList in descending order.
		Collections.sort(diagnostics, new Comparator<Diagnostic>() {
			@Override
			public int compare(Diagnostic d1, Diagnostic d2) {
				return d2.compareTo(d1);
			}
		});
		
		// If there's no clear Diagnostic, return an erroneous one
		if (diagnostics.get(0) == diagnostics.get(1) || diagnostics.get(0).getBias() == 0) {
			return new Diagnostic(new Disease("Keine feststellbare Krankheit", givenSymptoms, ""), 0);
		}

		return diagnostics.get(0);
	}

    // Method evoked by Symtoms_Anfrage to compute a diagnostic.
    public static void run(ArrayList<String> givenSymptoms)
    {
        initData();
        result = diagnose(givenSymptoms);
    }
}