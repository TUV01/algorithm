package types;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Diagnostic {
	private Disease disease;
	private int bias;
	private Date date;
	
	public Disease getDisease() {return disease;}
	public int getBias() {return bias;}
	public Date getDate() {return date;}
	public void setDisease(Disease other) {disease = other;}
	public void setBias(int other) {bias = other;}
	
	public String toString() {return getDisease().toString();}
	
	public Diagnostic(Disease disease, int bias)
	{
		this.disease = disease;
		this.bias = bias;
		this.date = new Date();
	}
	
	public int compareTo(Diagnostic other)
	{
		int thisBias = getBias();
		int otherBias = other.getBias();
		
		if (thisBias > otherBias) {return 1;}
		if (thisBias < otherBias) {return -1;}
		else return 0;
	}
}
