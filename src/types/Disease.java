package types;

import java.util.ArrayList;

public class Disease {
	private String name;
	private ArrayList<String> symptoms;
	private String treatment;
	private int symptomsCount = 0;

	public String getName() {return name;}
	public ArrayList<String> getSymptoms() {return symptoms;}
	public String getMedicine() {return treatment;}
	public int getSymptomsCount() {return symptomsCount;}
	public void setName(String other) {name = other;}
	public void setSymptoms(ArrayList<String> other) {symptoms = other;}
	public void setTreatment(String other) {treatment = other;}
	public String toString() {return name;}
	
	public Disease(String name, ArrayList<String> symptoms, String treatment)
	{
		this.name = name;
		this.symptoms = symptoms;
		this.treatment = treatment;
		for (String s : symptoms) {++symptomsCount;}
	}
	
	
}
