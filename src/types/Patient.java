package types;

import java.util.ArrayList;

/* This whole class is actually useless, if the DB connection doesn't work.
public class Patient {
	private String firstName;
	private String lastName;
	private ArrayList<Diagnostic> history;

	public String getFirstName() {return firstName;}
	public String getLastName() {return lastName;}
	public ArrayList<Diagnostic> getHistory() {return history;}

	public void setFirstName(String other) {firstName = other;}
	public void setLastName(String other) {lastName = other;}
	public void setHistory(ArrayList<Diagnostic> other) {history = other;}
	
	public String toString()
	{
		String out ="Name: " + firstName + " " + lastName + "\nGeburtsdatum: " + "\n\nHistorie:\n------------------------------------------\n";
		for (Diagnostic d : history) {out += d.toString() + "\n------------------------------------------\n";}
		return out;
	}
	
	public Patient(String firstName, String lastName)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		history = new ArrayList<Diagnostic>();
	}
	
	
	public void addToHistory(Diagnostic diagnostic)
	{history.add(diagnostic);}
}
*/